import * as React from "react"
import styled from "styled-components"
import useOnClickOutside from 'use-onclickoutside'

import { Button } from "@ui"

type OptionValue = string | number

type Option<T extends OptionValue> = {
  value: T
  label: string
}

type Props<T extends OptionValue> = {
  title: string
  options: Option<T>[]
  onChange: (values: Option<T>[]) => void
  isMulti?: boolean
  closeOnSelect?: boolean
}

export function Dropdown<T extends OptionValue>({
  title,
  options,
  onChange,
  isMulti = false,
  closeOnSelect = true,
}: Props<T>): React.ReactElement {
  const [headingText, setHeadingText] = React.useState<string>(title)
  const [isOpen, setIsOpen] = React.useState<boolean>(false)
  const [selected, setSelected] = React.useState<Option<T>[]>([])

  const ref = React.useRef(null)

  useOnClickOutside(ref, () => setIsOpen(false))

  React.useEffect(() => {
    const count = selected.length

    if (count === 0) {
      setHeadingText(title)
    }
    else if (count === 1) {
      setHeadingText(selected[0].label)
    }
    else {
      setHeadingText(`${count} selected`)
    }

    onChange(selected)
  }, [selected])

  const toggle = () => setIsOpen(!isOpen)

  const onOptionClicked = (option: Option<T>) => () => {
    let updatedState

    const index = selected.findIndex((selectedOption) => selectedOption.value === option.value)
    if (isMulti) {
      if (index !== -1) {
        updatedState = selected
          .slice(0, index)
          .concat(selected.slice(index + 1))
      } else {
        updatedState = [...selected, option]
      }
    }
    else {
      if (index !== -1) {
        updatedState = selected.slice(0, index)
      }
      else {
        updatedState = [option]
      }
    }

    setSelected(updatedState)

    if (closeOnSelect) {
      setIsOpen(false)
    }
  }

  return (
    <DropDownContainer ref={ref}>
      <DropDownHeader onClick={toggle} data-open={isOpen}>
        {headingText}
      </DropDownHeader>
      <DropDownList data-open={isOpen}>
        {options.map((option) => {
          const isSelected = Boolean(selected.find((selectedOption) => selectedOption.value === option.value))
          return (
            <ListItem
              onClick={onOptionClicked(option)}
              key={Math.random()}
              data-selected={isSelected}
            >
              {option.label}
            </ListItem>
          )
        })}
      </DropDownList>
    </DropDownContainer>
  )
}



const DropDownContainer = styled("div")`
  width: 12em;
  margin: 0 auto;
`

const DropDownHeader = styled(Button)`
  width: 100%;
  //box-shadow: 
  //  inset 0 0 15px rgba(55, 84, 170,0),
  //  inset 0 0 20px rgba(255, 255, 255,0),
  //  7px 7px 15px rgba(55, 84, 170,.15),
  //  -7px -7px 20px rgba(255, 255, 255,1),
  //  inset 0px 0px 4px rgba(255, 255, 255,.2);
  //
  //animation: click 699ms ease-in-out;
  //@keyframes click {
  //0%, 100% {
  //  box-shadow: 
  //    inset 0 0 15px rgba(55, 84, 170,0),
  //    inset 0 0 20px rgba(255, 255, 255,0),
  //    7px 7px 15px rgba(55, 84, 170,.15),
  //    -7px -7px 20px rgba(255, 255, 255,1),
  //    inset 0px 0px 4px rgba(255, 255, 255,.2);
  //}
  //50% {
  //  box-shadow: 
  //    inset 7px 7px 15px rgba(55, 84, 170,.15),
  //    inset -7px -7px 20px rgba(255, 255, 255,1),
  //    0px 0px 4px rgba(255, 255, 255,.2);
  //}
`

const DropDownList = styled("ul")`
  margin: 0;
  margin-top: 15px;
  font-size: 1.3rem;
  border-radius: var(--border-radius);
  box-shadow: var(--shadow-main);
  visibility: hidden;
  opacity: 0;
  transition: all 1s ease;

}
    
  &[data-open="true"] {
    visibility: visible;
    opacity: 1
  }
`

const ListItem = styled("li")`
  text-transform: capitalize;
  padding: 10px;
  cursor: pointer;
  transition: var(--shadow-transition);
  
  &[data-selected="true"] {
    box-shadow: var(--shadow-input-focused);
  }
`
