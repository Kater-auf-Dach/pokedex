import * as React from "react"
import styled, { css } from "styled-components"

interface Props<T> {
  values: T[]
  active: T
  onToggle: (value: T) => void
}

export function Toggler<T>({
  values,
  onToggle,
  active,
}: Props<T>): React.ReactElement {
  return (
    <Container>
      {values.map((value) => {
        const isActive = value === active
        const str = String(value)

        return (
          <Label data-checked={isActive} key={str}>
            <input
              type="radio"
              name={str}
              id={str}
              checked={isActive}
              onChange={() => onToggle(value)}
            />
            {value}
          </Label>
        )
      })}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  max-width: 300px;
  margin: 1em auto;

  & input {
    display: none;
  }
`

const Label = styled.label`
  position: relative;
  flex: 1;
  cursor: pointer;
  text-align: center;
  text-transform: uppercase;
  line-height: 50px;
  border: 1px solid rgba(255, 255, 255, 0.2);
  border-radius: 3px;
  box-shadow: var(--shadow-main-transitioned);
  transition: var(--shadow-transition);

  &:first-of-type {
    border-top-left-radius: 12px;
    border-bottom-left-radius: 12px;
  }

  &:last-of-type {
    border-top-right-radius: 12px;
    border-bottom-right-radius: 12px;
  }
  
  &[data-checked="true"] {
    box-shadow: inset 3px 3px 7px rgba(136, 165, 191, 0.48),
        inset -3px -3px 7px #fff;
  }
`
