import { useTheme } from "@lib/themes"
import * as React from "react"
import styled from "styled-components"

export const ThemeToggler = () => {
  const { isDark, toggle } = useTheme()

  return (
    <Label>
      <Toggler>
        {/*
      // @ts-ignore */}
        <Input type="checkbox" checked={isDark} onChange={toggle} />
        <Indicator />
      </Toggler>
      {/*
      // @ts-ignore */}
      <Lamp active={isDark} />
    </Label>
  )
}

const Label = styled.label`
  position: relative;
  display: inline-flex;
  align-items: center;
  margin-right: 0;
  cursor: pointer;
  color: #394a56;

  @media only screen and (min-width: 360px) {
    margin-right: 15px;
  }

  @media only screen and (min-width: 768px) {
    order: 3;
  }
`

const Toggler = styled.div`
  isolation: isolate;
  position: relative;
  height: 30px;
  width: 60px;
  border-radius: 15px;
  overflow: hidden;
  box-shadow: -8px -4px 8px 0px #ffffff, 8px 4px 12px 0px #d1d9e6,
    4px 4px 4px 0px #d1d9e6 inset, -4px -4px 4px 0px #ffffff inset;
`

const Indicator = styled.div`
  height: 100%;
  width: 200%;
  background: #ecf0f3;
  border-radius: 15px;
  transform: translate3d(-75%, 0, 0);
  transition: transform 0.4s cubic-bezier(0.85, 0.05, 0.18, 1.35);
  box-shadow: -8px -4px 8px 0px #ffffff, 8px 4px 12px 0px #d1d9e6;
`

const Lamp = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: -15px;
  margin: auto;
  width: 8px;
  height: 8px;
  background-color: #7a7a7a;
  border-radius: 50%;
  box-shadow: inset 1px 2px 0 rgba(0, 0, 0, 0.2);
  transition: all 0.2s linear;

  @media only screen and (max-width: 360px) {
    display: none;
  }

  // @ts-ignore
  ${(props) => props.active && "background-color: #55f696;"}
`

const Input = styled.input`
  display: none;

  &:checked + ${Indicator} {
    transform: translate3d(25%, 0, 0);
  }
`
