import { css } from "styled-components"

export const fonts = css`
  @font-face {
    font-family: "Oswald";
    font-style: normal;
    font-weight: 200;
    font-display: swap;
    src: url("/fonts/Oswald/Oswald-ExtraLight.ttf");
  }

  @font-face {
    font-family: "Oswald";
    font-style: normal;
    font-weight: 300;
    font-display: swap;
    src: url("/fonts/Oswald/Oswald-Light.ttf");
  }

  @font-face {
    font-family: "Oswald";
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: url("/fonts/Oswald/Oswald-Regular.ttf");
  }

  @font-face {
    font-family: "Oswald";
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: url("/fonts/Oswald/Oswald-Medium.ttf");
  }

  @font-face {
    font-family: "Oswald";
    font-style: normal;
    font-weight: 600;
    font-display: swap;
    src: url("/fonts/Oswald/Oswald-SemiBold.ttf");
  }

  @font-face {
    font-family: "Oswald";
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: url("/fonts/Oswald/Oswald-Bold.ttf");
  }
`
