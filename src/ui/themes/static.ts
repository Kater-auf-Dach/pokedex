import { css } from "styled-components"

export const staticTheme = css`
  --border-radius: 12px;

  @media only screen and (max-width: 680px) {
    font-size: 8px;
  }

  @media only screen and (min-width: 681px) {
    font-size: 10px;
  }
`
