import { css } from "styled-components"

export const lightTheme = css`
  --color-bg: #e0e5ec;
  --color-main: #fff;
  --color-text: rgba(201, 215, 230, 0.5);
  --color-active-bg: rgb(163, 177, 198);
  --text-shadow: 2px 2px 2px #d6e1ef99, 0 0 0 #000, 2px 2px 2px #d6e1ef00;

  --shadow-main: -7px -7px 15px rgba(255, 255, 255, 0.65),
    7px 7px 15px rgba(70, 70, 70, 0.12);
  --shadow-main-transitioned: inset 0 0 0 transparent, inset 0 0 0 transparent,
    var(--shadow-main);
  --shadow-input: 9px 9px 16px rgba(163, 177, 198, 0.6),
    -9px -9px 16px rgba(255, 255, 255, 0.5),
    inset 3px 3px 7px rgba(136, 165, 191, 0.48), inset -3px -3px 7px #fff;
  --shadow-input-focused: 0 0 0 transparent, 0 0 0 transparent,
    inset 3px 3px 7px rgba(136, 165, 191, 0.48), inset -3px -3px 7px #fff;

  --shadow-transition: box-shadow 0.5s ease-in;
`
