export { lightTheme } from "./light"
export { darkTheme } from "./dark"
export { staticTheme } from "./static"
