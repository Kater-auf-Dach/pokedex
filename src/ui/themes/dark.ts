import { css } from "styled-components"

export const darkTheme = css`
  --color-bg: #000;
  --color-text: rgba(201, 215, 230, 0.5);
`
