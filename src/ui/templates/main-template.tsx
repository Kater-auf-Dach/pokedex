import * as React from "react"
import styled, { css } from "styled-components"

export const MainTemplate = ({ header, footer, children }: any) => (
  <MainContainer>
    {header && <Header>{header}</Header>}
    <main>{children}</main>
    {footer && <Footer>{footer}</Footer>}
  </MainContainer>
)

export const MainContainer = styled.div`
  height: 100%;
  min-height: calc(100vh - 20px);
  background-color: #e0e5ec;
  padding: 1rem;
  border-radius: var(--border-radius);
  box-shadow: -7px -7px 15px rgba(255, 255, 255, 0.65),
    7px 7px 15px rgba(70, 70, 70, 0.12);
`

export const Header = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-bottom: 3rem;
`

const Footer = styled.footer`
  margin-top: 2rem;
`
