import { createGlobalStyle } from "styled-components"
import { darkTheme, lightTheme, staticTheme } from "./themes"
import { fonts } from "./fonts"

export const GlobalStyles = createGlobalStyle`
  :root {
    ${staticTheme}
  }

  [data-theme="light"] {
    ${lightTheme}
  }

  [data-theme="dark"] {
    ${darkTheme}
  }
  
  ${fonts}

  html {
    box-sizing: border-box;
    scroll-behavior: smooth;
  }

  *, *::before, *::after {
    box-sizing: inherit;
  }

  body {
      position: relative;
      min-height: 100%;
      margin: 0;
      padding: 10px;
      line-height: 1.2;
      font-size: 14px;
      font-family: 'Oswald', sans-serif;
      color: #3b4e76;
      background-color: var(--color-bg);
      // background-color: linear-gradient(318.32deg, #C3D1E4 0%, #DDE7F3 55%, #D4E0ED 100%);
      color: rgba(201, 215, 230, 0.5);
      text-shadow: 2px 2px 2px #d6e1ef99, 0 0 0 #000, 2px 2px 2px #d6e1ef00;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-weight: 700;
    }
    
    a {
      text-decoration: none;
    
      &:hover {
        text-decoration: underline;
      }
    }


    .content {
      display: flex;
    }
 
    button {
      border: none;
      margin: 0;
      padding: 0;
      width: auto;
      overflow: visible;


      background: transparent;

      /* inherit font & color from ancestor */
      color: inherit;
      font: inherit;

      /* Normalize \`line-height\`. Cannot be changed from \`normal\` in Firefox 4+. */
      line-height: normal;

      /* Corrects font smoothing for webkit */
      -webkit-font-smoothing: inherit;
      -moz-osx-font-smoothing: inherit;

      /* Corrects inability to style clickable \`input\` types in iOS */
      -webkit-appearance: none;
    }

    ul {
      padding: 0;
      list-style: none;
    }

    img {
      max-width: 100%;
      height: auto;
    }
`
