import styled from "styled-components"

export const H1 = styled.h1`
  padding: 1rem 0;
  margin: 0;
  font-size: 32px;
  user-select: none;
`
