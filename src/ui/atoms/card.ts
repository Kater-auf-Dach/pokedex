import styled from "styled-components"
import { Link } from "react-router-dom"

export const Card = styled.li`
  min-height: 160px;
  padding: 10px;
  text-align: center;
  box-shadow: var(--shadow-main);
`

export const CardLink = styled(Link)`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  min-height: 140px;
  font-size: 18px;
  color: var(--color-text);
  text-decoration: none;
  text-transform: capitalize;
  text-shadow: var(--text-shadow);
`
