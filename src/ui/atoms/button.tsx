import styled, { css } from "styled-components"

export const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  line-height: 50px;
  color: var(--color-text);
  border-radius: var(--border-radius);
  box-shadow: var(--shadow-main-transitioned);
  text-shadow: var(--text-shadow);
  border: 1px solid rgba(255, 255, 255, 0.2);
  transition: all 0.5s ease-in;

  &:hover {
    background: linear-gradient(145deg, #caced4, #f0f5fd);
  }
  
  &:active,
  &:focus {
    outline: none;
  }
  
  &:disabled {
    text-shadow: none;
  }

  ${(props) =>
    // @ts-ignore
    props.active &&
    css`
      background: #e0e5ec;
      box-shadow: inset 4px 4px 8px #bec3c9, inset -4px -4px 8px #fff;
    `}
  }
`
