import styled from "styled-components"

export const Container = styled.div`
  height: 100%;
  background-color: #e0e5ec;
  padding: 1rem;
  border-radius: var(--border-radius);
`
