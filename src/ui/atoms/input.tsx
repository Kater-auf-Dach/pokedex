import styled from "styled-components"

export const Input = styled.input`
  display: block;
  width: 75%;
  max-width: 400px;
  line-height: 50px;
  border: 0;
  border-radius: var(--border-radius);
  padding: 0 15px;
  font-size: 18px;
  outline: none;
  background: linear-gradient(
    318.32deg,
    rgba(163, 177, 198, 0.1) 0%,
    rgba(163, 177, 198, 0.1) 55%,
    rgba(163, 177, 198, 0.25) 100%
  );
  box-shadow: var(--shadow-input);
  transition: var(--shadow-transition);

  &:focus {
    box-shadow: var(--shadow-input-focused);
  }
`
