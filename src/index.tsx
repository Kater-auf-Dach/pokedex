import * as React from "react"
import * as ReactDom from "react-dom"
import { Router } from "react-router"
// import { createInspector } from "effector-logger"

import { history } from "@lib/routing"
import { App } from "./app"

const root = document.querySelector("#root")

const render = () => {
  if (root) {
    ReactDom.render(
      <Router history={history}>
        <App />
      </Router>,
      root,
    )
  }
}

if ((module as any).hot) {
  ;(module as any).hot.accept("./app", render)
}

render()

// createInspector()
