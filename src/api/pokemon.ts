import { request } from "@lib/http-service"

const getPokemon = (name: string) => request("GET", `/pokemon/${name}`)
const getSpecies = (id: number) => request("GET", `/pokemon-species/${id}`)

export const pokemonApi = {
  getPokemon,
  getSpecies,
}
