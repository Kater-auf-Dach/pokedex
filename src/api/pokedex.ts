import { request } from "@lib/http-service"

const getList = (
  offset: number = 0,
  limit: number = 0,
): Promise<PokedexResponse> =>
  request("GET", `/pokemon?offset=${offset}&limit=${limit}`)

const getAll = (limit: number): Promise<PokedexResponse> =>
  request("GET", `/pokemon?limit=${limit}`)

const getTypes = (limit: number = 0): Promise<PokemonTypeResponse> =>
  request("GET", `/type?limit=${limit}`)

const getByType = (type: string | number): Promise<any> =>
  request("GET", `/type/${type}`)

export const pokedexApi = {
  getList,
  getAll,
  getTypes,
  getByType,
}
