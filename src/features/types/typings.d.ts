type PokemonTypeInitial = {
  name: string
  url: string
}

type PokemonType = {
  name: string
  id: number
}

type PokemonTypeResponse = {
  count: number
  results: PokemonTypeInitial[]
}

type TypeRegistry = { [id: number]: PokemonType }