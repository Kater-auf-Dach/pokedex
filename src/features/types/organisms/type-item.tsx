import { Card, CardLink } from "@ui"
import * as React from "react"
import styled from "styled-components"

interface Props {
  type: PokemonType
}

export const TypeItem: React.FC<Props> = ({ type }): React.ReactElement => (
  <Card key={type.name}>
    <CardLink to={`/types/${type.name}`}>
      <span>{type.name}</span>
      <ImageContainer>
        <img
          // src={`https://raw.githubusercontent.com/duiker101/pokemon-type-svg-icons/master/icons/${type.name}.svg`}
          src={`https://maurowernly.github.io/Pokedex/images/types/${type.name
            .charAt(0)
            .toUpperCase() + type.name.slice(1)}.png`}
          alt={`${type.name} icon`}
        />
      </ImageContainer>
    </CardLink>
  </Card>
)

const ImageContainer = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  //box-shadow: 0 0 20px #0c69c8;
  box-shadow: var(--shadow-main);

  & > img {
    height: 60%;
    width: 60%;
    margin: 20%;
  }
`
