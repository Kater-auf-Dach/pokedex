import * as React from "react"
import { useStoreMap } from "effector-react"
import styled from "styled-components"

import { ConditionalList } from "@ui"
import { TypeItem } from "./type-item"
import { $typesRegistry } from "../model"

type Props = {
  ids: number[]
  renderEmpty?: () => React.ReactElement | null
}

export const TypesList: React.FC<Props> = ({ ids, renderEmpty = emptyRenderer }): React.ReactElement => (
  <ConditionalList
    list={ids}
    renderEmpty={renderEmpty}
    renderExists={(typesIds) => (
      <TypesItemsBlock>
        {typesIds.map((typeId) => (
          <TypeComponent id={typeId} key={typeId} />
        ))}
      </TypesItemsBlock>
    )}
  />
)

type TypeComponentProps = {
  id: number
}

const TypeComponent: React.FC<TypeComponentProps> = ({ id }): React.ReactElement => {
  const type = useStoreMap({
    store: $typesRegistry,
    keys: [id],
    fn: (types, [typeId]) => types[typeId],
  })

  return <TypeItem type={type} />
}

const emptyRenderer = () => <p>No types in list</p>

export const TypesItemsBlock = styled.ul`
  display: grid;
  grid-column-gap: 30px;
  grid-row-gap: 30px;
  grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
`
