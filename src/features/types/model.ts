import { pokedexApi } from "@api/pokedex"
import { convertUrlToId, itemsToObject } from "@lib/registry"
import {
  Effect,
  createEffect,
  createEvent,
  createStore,
  forward,
  sample,
} from "effector-logger"

export const loadPokemonsTypesFx: Effect<
  any,
  PokemonTypeResponse,
  void
> = createEffect()
export const loadPokemonsByTypeFx: Effect<any, any, void> = createEffect()

loadPokemonsTypesFx.use(() => pokedexApi.getTypes())
loadPokemonsByTypeFx.use((type) => pokedexApi.getByType(type))

export const typeSelected = createEvent()
export const typeCleared = createEvent()

export const $types = createStore<PokemonType[]>([])
export const $typesRegistry = createStore<Registry>({})
export const $typesIds = createStore<number[]>([])
export const $renderType = createStore<boolean>(false)

$types.on(loadPokemonsTypesFx.done, (_, { result }) =>
  result.results.map((type) => convertUrlToId(type)),
)

forward({
  from: typeSelected,
  to: loadPokemonsByTypeFx,
})

$renderType.on(typeSelected, (_, payload) => true)
$renderType.on(typeCleared, (_, payload) => false)

sample({
  // @ts-ignore
  source: $types,
  fn: itemsToObject,
  target: $typesRegistry,
})

sample({
  // @ts-ignore
  source: $types,
  fn: (types: PokemonType[]) => types.map((type) => type.id),
  target: $typesIds,
})
