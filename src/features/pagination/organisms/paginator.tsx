import * as React from "react"
import styled, { css } from "styled-components"
import { Button } from "@ui"

export const Paginator: React.FunctionComponent<{
  total: number
  initial: number
  ellipsis?: string
  gotoPage: (page: number) => void
}> = ({ total, initial = 1, ellipsis = "...", gotoPage }) => {
  const [list, setRange] = React.useState<Array<string | number>>([])
  const [current, setCurrent] = React.useState(initial)

  React.useEffect(() => {
    setCurrent(initial)
  }, [initial])

  React.useEffect(() => {
    setRange(pagination(current, total))
  }, [current, total])

  const setPage = React.useCallback(
    (num: number) => {
      if (num === current) {
        return
      }
      setRange(pagination(num, total))
      setCurrent(num)

      gotoPage(num)
    },
    [current],
  )

  const goToPrev = () => {
    if (current === 1) {
      return
    }
    setPage(current - 1)
  }

  const goToNext = () => {
    if (current === total) {
      return
    }
    setPage(current + 1)
  }

  if (!total || total < 2) {
    return null
  }

  return (
    <PaginationBlock>
        {list.map((el, i) => {
          if (el === dotSymbol) {
            return (
              <PageButton key={i} disabled>
                {ellipsis}
              </PageButton>
            )
          }

          return (
            <PageButton
              key={i}
              //@ts-ignore
              active={current === el}
              onClick={() => setPage(el as number)}
            >
              {el}
            </PageButton>
          )
        })}
    </PaginationBlock>
  )
}

const PaginationBlock = styled.ul`
  display: flex;
  justify-content: space-between;
  width: 100%;
  max-width: 400px;
  margin: 2rem auto;
  padding: 1rem;
  order: 2;

  border-radius: 12px;
  border: 1px solid rgba(255, 255, 255, 0.2);
  box-shadow: var(--shadow-main);
`

const PageButton = styled(Button)`
  width: 40px;
  height: 40px;
  color: #000000;
`
function getRange(from: number, to: number, step = 1) {
  let i = from
  const range: number[] = []

  while (i <= to) {
    range.push(i)
    i += step
  }

  return range
}

const dotSymbol = "..."

const pagination = (currentPage: number, pageCount: number) => {
  let delta
  if (pageCount <= 7) {
    delta = 7
  } else {
    delta = currentPage > 4 && currentPage < pageCount - 3 ? 2 : 4
  }

  const range = {
    start: Math.round(currentPage - delta / 2),
    end: Math.round(currentPage + delta / 2),
  }

  if (range.start - 1 === 1 || range.end + 1 === pageCount) {
    range.start += 1
    range.end += 1
  }

  let pages: any =
    currentPage > delta
      ? getRange(
      Math.min(range.start, pageCount - delta),
      Math.min(range.end, pageCount),
      )
      : getRange(1, Math.min(pageCount, delta + 1))

  const withDots = (value: number, pair: [string | number, string | number]) =>
    pages.length + 1 !== pageCount ? pair : [value]

  if (pages[0] !== 1) {
    pages = withDots(1, [1, dotSymbol]).concat(pages)
  }

  if (pages[pages.length - 1] < pageCount) {
    pages = pages.concat(withDots(pageCount, [dotSymbol, pageCount]))
  }

  return pages
}

