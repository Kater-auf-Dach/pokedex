import * as React from "react"
import { useStore } from "effector-react"

import {
  $totalPages,
  pageChanged,
  pageLimitChanged,
  paginationMounted,
} from "../model"
import { Paginator } from "./paginator"

// eslint-disable-next-line no-magic-numbers
const PER_PAGE_LIST = [20, 40, 60]
const INITIAL_PER_PAGE = PER_PAGE_LIST[0]

export function paginate(array: any[], pageSize: number, pageNumber: number) {
  return array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize)
}

interface PaginationProps {
  list: any[]
}

export const Pagination: React.FC<PaginationProps> = ({
  list,
}): React.ReactElement => {
  const totalPages = useStore($totalPages)

  React.useEffect(() => {
    if (list && list.length > INITIAL_PER_PAGE) {
      paginationMounted(list)
    }

    pageLimitChanged(INITIAL_PER_PAGE)
  }, [list])

  return <Paginator gotoPage={pageChanged} total={totalPages} initial={1} />
}
