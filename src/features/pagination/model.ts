import { combine, createEvent, createStore } from "effector-logger"

function paginate(current: number = 1, length: number = 1) {
  const last = length
  const delta = 2
  const left = current - delta
  const right = current + delta + 1
  const range = []
  const rangeWithDots = []
  let l

  for (let i = 1; i <= last; i++) {
    if (i === 1 || i === last || (i >= left && i < right)) {
      range.push(i)
    }
  }

  for (const i of range) {
    if (l) {
      if (i - l === 2) {
        rangeWithDots.push(l + 1)
      } else if (i - l !== 1) {
        rangeWithDots.push("...")
      }
    }
    rangeWithDots.push(i)
    l = i
  }

  return rangeWithDots
}

export const paginationMounted = createEvent<any[]>()
export const pageLimitChanged = createEvent<number>()
export const pageChanged = createEvent<number>()

const $list = createStore<any[]>([])
export const $totalRecords = createStore<number>(0)
export const $pageLimit = createStore<number>(0)
export const $currentPage = createStore<number>(1)

export const $totalPages = combine(
  $totalRecords,
  $pageLimit,
  (records, limit) => (records && limit ? Math.ceil(records / limit) : 1),
)
export const $range = combine($currentPage, $totalPages, (current, total) =>
  paginate(current, total),
)
export const $currentRecords = combine(
  $list,
  $pageLimit,
  $currentPage,
  (list, pageLimit, currentPage) => {
    const offset = (currentPage - 1) * pageLimit

    return list.slice(offset, offset + pageLimit)
  },
)

$list.on(paginationMounted, (_, list) => list)
$totalRecords.on(paginationMounted, (_, list) => list.length)
$pageLimit.on(pageLimitChanged, (_, pageLimit) => pageLimit)
$currentPage.on(pageChanged, (_, number) => number)
$currentPage.on(pageLimitChanged, (_, number) => 1)
