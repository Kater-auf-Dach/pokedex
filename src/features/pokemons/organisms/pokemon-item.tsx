import { Card, CardLink } from "@ui"
import * as React from "react"

interface Props {
  pokemon: Pokemon
}

export const PokemonItem: React.FC<Props> = ({
  pokemon,
}): React.ReactElement => (
  <Card key={pokemon.name}>
    <CardLink to={`/pokemon/${pokemon.name}`}>
      <span>{pokemon.name}</span>
      <img
        src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`}
        alt={`${pokemon.name} icon`}
      />
    </CardLink>
  </Card>
)
