import * as React from "react"
import { useStoreMap } from "effector-react"
import styled from "styled-components"

import { ConditionalList } from "@ui"
import { PokemonItem } from "./pokemon-item"
import { $pokemonRegistry } from "../model"

type Props = {
  ids: number[]
  renderEmpty?: () => React.ReactElement | null
}

export const PokemonsList: React.FC<Props> = ({ ids, renderEmpty = emptyRenderer }): React.ReactElement => (
  <ConditionalList
    list={ids}
    renderEmpty={renderEmpty}
    renderExists={(pokemonsIds) => (
      <PokemonsItemsBlock>
        {pokemonsIds.map((pokemonId) => (
          <PokemonComponent id={pokemonId} key={pokemonId} />
        ))}
      </PokemonsItemsBlock>
    )}
  />
)

type PokemonComponentProps = {
  id: number
}

const PokemonComponent: React.FC<PokemonComponentProps> = ({ id }): React.ReactElement => {
  const pokemon = useStoreMap({
    store: $pokemonRegistry,
    keys: [id],
    fn: (allPokemons, [pokemonId]) => allPokemons[pokemonId],
  })

  return <PokemonItem pokemon={pokemon} />
}

const emptyRenderer = () => <p>No pokemons in list</p>

export const PokemonsItemsBlock = styled.ul`
  display: grid;
  grid-column-gap: 30px;
  grid-row-gap: 30px;
  grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
`
