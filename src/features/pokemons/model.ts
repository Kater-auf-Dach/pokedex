import { itemsToObject } from "@lib/registry"
import { createStore, sample } from "effector-logger"

export const $pokemonRegistry = createStore<Registry>({})
export const $allPokemons = createStore<Pokemon[]>([])

sample({
  // @ts-ignore
  source: $allPokemons,
  fn: itemsToObject,
  target: $pokemonRegistry,
})
