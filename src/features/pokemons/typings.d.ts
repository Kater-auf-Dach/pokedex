type PokemonInitial = {
  name: string
  url: string
}

type Pokemon = {
  name: string
  id: number
}

type PokedexResponse = {
  count: number
  next: string | null
  previous: string | null
  results: PokemonInitial[]
}

type Registry = { [id: number]: Pokemon }

