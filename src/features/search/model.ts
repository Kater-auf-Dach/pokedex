import { $allPokemons } from "@features/pokemons"
import { combine, createEvent, createStore, sample } from "effector-logger"

export const searchTriggered = createEvent<string>()
export const searchCleared = createEvent()
export const searchTypeChanged = createEvent<SearchType>()

export const $searchPokemonsIds = createStore<number[]>([])
export const $searchType = createStore<SearchType>("name")

const findPokemons = (pokemons: Pokemon[], searchType: string, query: string) =>
  pokemons
    .filter((pokemon) => {
      if (searchType === "name") {
        return pokemon.name.includes(query.toLowerCase())
      }
      return String(pokemon.id).includes(query)
    })
    .map((pokemon) => pokemon.id)

const $search = combine(
  { pokemons: $allPokemons, searchType: $searchType },
  ({ pokemons, searchType }) => ({
    pokemons,
    searchType,
  }),
)

sample({
  source: $search,
  clock: searchTriggered,
  fn: ({ pokemons, searchType }, query) =>
    findPokemons(pokemons, searchType, query),
  target: $searchPokemonsIds,
})

$searchPokemonsIds.on(searchCleared, () => $searchPokemonsIds.defaultState)
$searchType.on(searchTypeChanged, (_, payload) => payload)
