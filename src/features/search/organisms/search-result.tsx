import { PokemonsList } from "@features/pokemons"
import * as React from "react"
import { useStore } from "effector-react"

import { $searchPokemonsIds } from "../model"

export const SearchResult: React.FC = () => {
  const ids = useStore($searchPokemonsIds)

  return <PokemonsList ids={ids} renderEmpty={() => null} />
}
