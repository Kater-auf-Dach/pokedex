import { useDebouncedValue } from "@lib/debounce-hook"
import { Button, Input, Toggler } from "@ui"
import * as React from "react"
import { useStore } from "effector-react"
import styled from "styled-components"

import {
  $searchType,
  searchCleared,
  searchTriggered,
  searchTypeChanged,
} from "../model"

const UPDATE_INTERVAL = 500

export const Search: React.FC = () => {
  const [searchString, setSearchString] = React.useState("")
  const searchType = useStore($searchType)

  const debouncedSearchString = useDebouncedValue(searchString, UPDATE_INTERVAL)

  const inputRef = React.useRef(null)

  React.useEffect(() => {
    if (debouncedSearchString) {
      searchTriggered(debouncedSearchString)
    } else {
      searchCleared()
    }
  }, [debouncedSearchString])

  const handler = (e: React.ChangeEvent<HTMLInputElement>) =>
    setSearchString(e.target.value)

  const handlerClear = () => {
    setSearchString("")
    searchCleared()
  }

  const setSearchType = (value: SearchType) => {
    searchTypeChanged(value)
    handlerClear()

    // @ts-ignore
    inputRef.current.focus()
  }

  const values: SearchType[] = ["number", "name"]
  const inputType = searchType === "name" ? "text" : "number"
  const inputPlaceholder =
    searchType === "name" ? "Type the name" : "Type the id"

  return (
    <SearchContainer>
      <Toggler values={values} onToggle={setSearchType} active={searchType} />
      <Input
        type={inputType}
        onChange={handler}
        value={searchString}
        placeholder={inputPlaceholder}
        spellCheck="false"
        ref={inputRef}
      />
      <Button onClick={handlerClear} disabled={!debouncedSearchString}>
        Clear
      </Button>
    </SearchContainer>
  )
}

const SearchContainer = styled.div`
  grid-area: search;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-bottom: 2em;

  & > ${Button} {
      width: 50px;
      margin-left: 20px;
      font-weight: 600;
    }
  }
`
