import { Store } from "effector-logger"

export function getIdFromUrl(url: string): number {
  return parseInt(url.match(/([^\/]*)\/*$/)![1], 10)
}

export function getFromRegistry(registry: Store<any>, id: number) {
  return registry.getState()[id]
}

type InitialItem = {
  name: string
  url: string
}

type RegistryItem = {
  name: string
  id: number
}

export function convertUrlToId(item: InitialItem): RegistryItem {
  return ({
    name: item.name,
    id: getIdFromUrl(item.url),
  })
}

export function itemsToObject(list: RegistryItem[]) {
  return list.reduce((object, item) => {
    object[item.id] = item
    return object
  }, {} as Record<number, RegistryItem>)
}