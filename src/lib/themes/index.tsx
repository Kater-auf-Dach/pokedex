import * as React from "react"
import { combine } from "effector"
import { useStore } from "effector-react"

import { $prefersDark } from "./system-prefers"
import { $selectedTheme, themeToggled } from "./user-prefers"

const $theme = combine(
  $selectedTheme,
  $prefersDark,
  (selected, prefersDark) => {
    if (selected === "auto") {
      return prefersDark ? "dark" : "light"
    }

    return selected
  },
)

const $isDark = $theme.map((theme) => theme === "dark")

export const ToggleThemeProvider = ({ children }: any) => {
  const isDark = useStore($isDark)

  React.useEffect(() => {
    const html = document.querySelector("html")
    if (html) {
      html.dataset.theme = isDark ? "dark" : "light"
    }
  }, [isDark])

  return <>{children}</>
}

export const useTheme = () => {
  const theme = useStore($selectedTheme)
  const isDark = useStore($isDark)

  return { theme, isDark, toggle: themeToggled }
}
