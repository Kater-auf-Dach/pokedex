import { createEvent, restore } from "effector"

const matcher = window.matchMedia("(prefers-color-scheme: dark)")

const prefersChanged = createEvent<boolean>()

export const $prefersDark = restore<boolean>(prefersChanged, matcher.matches)

try {
  matcher.addEventListener("change", (event) => {
    prefersChanged(event.matches)
  })
} catch (e1) {
  try {
    matcher.addListener((event) => {
      prefersChanged(event.matches)
    })
  } catch (e2) {
    console.error(e2)
  }
}
