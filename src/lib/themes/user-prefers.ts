import { createEvent, createStore } from "effector"

export const themeToggled = createEvent<void>()

type Theme = "dark" | "light" | "auto"
const availableThemes: Theme[] = ["dark", "light", "auto"]

export const $selectedTheme = createStore<Theme>(restoreTheme())

$selectedTheme.on(themeToggled, toNextTheme)

$selectedTheme.watch(saveTheme)

function restoreTheme(): Theme {
  const theme = localStorage.getItem("theme")
  for (const available of availableThemes) {
    if (available === theme) {
      return available
    }
  }
  return "light"
}

function saveTheme(theme: Theme) {
  localStorage.setItem("theme", theme)
}

const nextTheme: any = {
  // auto: "dark",
  dark: "light",
  // light: "auto",
  light: "dark",
}

function toNextTheme(theme: string) {
  return nextTheme[theme]
}
