export function createNumberFromId(id: number): string {
  if (id < 10) {
    return `00${id}`
  }

  if (id < 100) {
    return `0${id}`
  }
  return id.toString()
}

export function formatName(name: string): string {
  return name.charAt(0).toUpperCase() + name.slice(1)
}
