import { GlobalStyles } from "@ui"
import { ToggleThemeProvider } from "@lib/themes"
import * as React from "react"
import { hot } from "react-hot-loader"

import { Normalize } from "styled-normalize"
import { Pages } from "./pages"

export const App = hot(module)(() => (
  <ToggleThemeProvider>
    <Normalize />
    <GlobalStyles />
    <Pages />
  </ToggleThemeProvider>
))
