import { path } from "./paths"

import { HomePage } from "./home"
import { PokemonPage } from "./pokemon"
import { TypePage } from "./type"
import { NotFoundPage } from "./404"

export const routes = [
  {
    path: path.home(),
    exact: true,
    component: HomePage,
  },
  {
    path: path.singlePokemon(":name"),
    exact: true,
    component: PokemonPage,
  },
  {
    path: path.singleType(":name"),
    exact: true,
    component: TypePage,
  },
  {
    path: "*",
    component: NotFoundPage,
  },
]
