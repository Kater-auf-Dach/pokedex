import * as React from "react"
import styled from "styled-components"

export const NotFoundPage: React.FC = (): React.ReactElement => (
  <NotFoundContainer>404</NotFoundContainer>
)

const NotFoundContainer = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: center;
  justify-content: center;
  font-size: 4rem;
`
