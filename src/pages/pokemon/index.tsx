import { MainTemplate, ThemeToggler } from "@ui"
import { createNumberFromId, formatName } from "@lib/formatters"
import * as React from "react"
import { Link, useParams } from "react-router-dom"
import { useStore } from "effector-react"
import styled from "styled-components"
import { $pokemon, $stats, $types, pageMounted, pageUnmounted } from "./model"

// type Props = {
//   match: {
//     params: {
//       username: string
//     }
//   }
// }

export const PokemonPage: React.FC = () => {
  const { name } = useParams()
  // const { name } = match.params

  const pokemon = useStore($pokemon)
  const types = useStore($types)
  const stats = useStore($stats)

  React.useEffect(() => {
    pageMounted({ name })

    return () => pageUnmounted()
  }, [name])

  return (
    <>
      <MainTemplate header={<Header />}>
        {pokemon ? (
          <>
            <HeaderLinksContainer>
              <HeaderLinks to={`/pokemon/${pokemon.id - 1}`}>{"<"}</HeaderLinks>
              <HeaderLinks to={`/pokemon/${pokemon.id + 1}`}>{">"}</HeaderLinks>
            </HeaderLinksContainer>
            {/* <div style={{ backgroundColor: color || "transparent" }}> */}
            <div>
              <Title>
                #{createNumberFromId(pokemon.id)} {formatName(pokemon.name)}
              </Title>
              <Avatar>
                <img
                  src={`https://maurowernly.github.io/Pokedex/images/pokemons/${createNumberFromId(
                    pokemon.id,
                  )}${formatName(pokemon.name)}.png`}
                  alt=""
                />
              </Avatar>
              <Stats>
                <StatsTypes>
                  {types &&
                    types.map((type) => (
                      <StatsTypeItem>
                        <img
                          src={`https://maurowernly.github.io/Pokedex/images/types/${type
                            .charAt(0)
                            .toUpperCase() + type.slice(1)}.png`}
                          alt=""
                        />
                      </StatsTypeItem>
                    ))}
                </StatsTypes>
                {stats && (
                  <>
                    <StatsTitle>Base stats</StatsTitle>
                    <StatsLine />
                    <StatsBaseList>
                      {stats.map(({ stat, value }) => (
                        <StatBaseItem>
                          <StatBaseTitle>{stat}</StatBaseTitle>
                          <Progress>
                            <ProgressBack />
                            {/*
                          // @ts-ignore */}
                            <ProgressLine width={value} />
                          </Progress>
                          <StatBaseValue>{value}</StatBaseValue>
                        </StatBaseItem>
                      ))}
                    </StatsBaseList>
                  </>
                )}
              </Stats>
              {/* <Link to="/">To home page</Link> */}
            </div>
          </>
        ) : (
          <h2>Loading...</h2>
        )}
      </MainTemplate>
    </>
  )
}

const Header = () => (
  <>
    <Heading to="/">Pokedex</Heading>
    <ThemeToggler />
  </>
)

const Heading = styled(Link)`
  padding: 1rem 0;
  margin: 0;
  font-size: 32px;
  user-select: none;
`

const Title = styled.h2`
  margin: 0;
  padding: 12.5px;
  text-align: center;
  text-transform: uppercase;
  //border-radius: 12px;
  //border: 1px solid rgba(255, 255, 255, 0.2);
  //box-shadow: var(--shadow-main);
`

const HeaderLinksContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 1em auto;

  & input {
    display: none;
  }
`

const HeaderLinks = styled(Link)`
  position: relative;
  width: 50px;
  text-align: center;
  text-transform: uppercase;
  line-height: 50px;
  cursor: pointer;
  text-decoration: none;
  border: 1px solid rgba(255, 255, 255, 0.2);
  border-radius: 3px;
  box-shadow: var(--shadow-main-transitioned);
  transition: var(--shadow-transition);

  &:first-of-type {
    border-top-left-radius: 12px;
    border-bottom-left-radius: 12px;
    margin-right: 20px;
  }

  &:last-of-type {
    border-top-right-radius: 12px;
    border-bottom-right-radius: 12px;
  }

  &[data-checked="true"] {
    box-shadow: inset 3px 3px 7px rgba(136, 165, 191, 0.48),
      inset -3px -3px 7px #fff;
  }
`

const Avatar = styled.div`
  margin: 3rem;
`

const Stats = styled.div`
  grid-area: stats;
  padding: 10px;
  border-radius: var(--border-radius);
  box-shadow: var(--shadow-main);
`

const StatsTypes = styled.ul`
  display: flex;
`

const StatsTypeItem = styled.li`
  border-radius: 50%;
  width: 80px;
  height: 80px;
  padding: 10px;
  margin-right: 40px;
  margin-bottom: 50px;
  border: 2px solid #e0e5ec;
  box-shadow: inset 0 0 4px rgba(255, 255, 255, 0),
    inset 3px 3px 5px rgba(55, 84, 170, 0.15),
    inset -3px -3px 5px rgba(255, 255, 255, 0.5),
    0px 0px 4px rgba(255, 255, 255, 0.2);
`

const StatsTitle = styled.h3`
  font-size: 24px;
  font-weight: 600;
  padding-bottom: 10px;
`

const StatsLine = styled.div`
  width: 100%;
  height: 5px;
  box-shadow: 0 1px 4px 0 rgba(255, 255, 255, 0.25);
`

const StatsBaseList = styled.ul`
  display: grid;
  align-items: center;
  grid-row-gap: 10px;
`

const StatBaseItem = styled.li`
  display: grid;
  align-items: center;
  grid-template-columns: 2fr auto 1fr;
  font-weight: 400;
  font-size: 18px;
`

const StatBaseTitle = styled.div`
  padding-bottom: 10px;
`

const StatBaseValue = styled.div`
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  border: 2px solid #e0e5ec;
  box-shadow: inset 0 0 4px rgba(255, 255, 255, 0),
    inset 3px 3px 5px rgba(55, 84, 170, 0.15),
    inset -3px -3px 5px rgba(255, 255, 255, 0.5),
    0 0 4px rgba(255, 255, 255, 0.2);
`

const Progress = styled.div`
  max-width: 300px;
  min-width: 100px;
  margin: 0 30px;
  // &-back {
  //   height: 24px;
  //   background-color: #EBECF0;
  //   border-radius: 10px;
  //   border: 4px solid #f3f4f7;
  //   box-shadow:
  //   7px 7px 15px rgba(55, 84, 170, .15),
  //   -7px -7px 20px rgba(255, 255, 255, 1),
  //   inset 0px 0px 4px rgba(255, 255, 255, 0),
  //   inset 7px 7px 15px rgba(55, 84, 170, .15),
  //   inset -7px -7px 20px rgba(255, 255, 255, 1),
  //   0px 0px 4px rgba(255, 255, 255, .2);
  // }

  // &-line {
  //   width: 40%;
  //   height: 16px;
  //   background-color: #185BF1;
  //   margin-top: -20px;
  //   margin-left: 4px;
  //   border-radius: 8px;
  // }
`

const ProgressBack = styled.div`
  height: 10px;
  background-color: #ebecf0;
  border-radius: 10px;
  box-shadow: -5px -5px 10px 0px white, 5px 5px 10px 0px #a0aabc;
  // box-shadow: -5px -5px 10px 0px rgba(255, 255, 255, 1), 5px 5px 10px 0px rgba(160, 170, 188, 1);
`

const ProgressLine = styled.div`
  height: 10px;
  background-image: linear-gradient(240deg, #37dbe0 0%, #1758f1 100%);
  margin-top: -10px;
  border-radius: 12px;

  // @ts-ignore
  width: ${(props) => props.width || "0"}%;
`
