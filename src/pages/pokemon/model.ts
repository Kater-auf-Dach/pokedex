import { pokemonApi } from "@api/pokemon"
import { Effect, createEffect, createStore, forward, createEvent } from "effector-logger"

type Pokemon = {
  name: string
  weight: number
  height: number
  id: number
  types: []
  stats: []
  color: string
}

export const pageMounted = createEvent<{ name: string | undefined }>()
export const pageUnmounted = createEvent()

export const $pokemon = createStore<Pokemon | null>(null)
export const $species = createStore(null)
export const $types = $pokemon.map((pokemon) => pokemon && pokemon.types.map((typeSlot: any) => typeSlot.type.name))
export const $stats = $pokemon.map((pokemon) => pokemon && pokemon.stats.map((statSlot: any) => ({
    stat: statSlot.stat.name,
    value: statSlot.base_stat,
  })
))
// @ts-ignore
// export const $color = $species.map((species) => species?.color?.name)

export const loadPokemonFx: Effect<any, any, void> = createEffect()
export const loadSpeciesFx: Effect<any, any, void> = createEffect()

loadPokemonFx.use(({ name }: { name: string }) => pokemonApi.getPokemon(name))
loadSpeciesFx.use((id: number) => pokemonApi.getSpecies(id))

$pokemon.on(loadPokemonFx.done, (_, { result }) => result)
$species.on(loadSpeciesFx.done, (_, { result }) => result)

$pokemon.on(pageUnmounted, (_, result) => null)
$species.on(pageUnmounted, (_, result) => null)


forward({
  from: pageMounted,
  to: loadPokemonFx,
})

forward({
  from: loadPokemonFx.done.map(({ result }) => result.id),
  to: loadSpeciesFx,
})
