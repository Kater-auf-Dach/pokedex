import { pokedexApi } from "@api/pokedex"
import { convertUrlToId, getFromRegistry } from "@lib/registry"
import { $allPokemons, $pokemonRegistry } from "@features/pokemons"
import {
  $searchPokemonsIds,
  searchCleared,
  searchTriggered,
} from "@features/search"
import { $currentRecords } from "@features/pagination"
import { loadPokemonsTypesFx } from "@features/types"
import {
  Effect,
  createEffect,
  createEvent,
  createStore,
  forward,
  sample,
} from "effector-logger"
import { createGate } from "effector-react"

export const Gate = createGate()

export const loadPokemonsFx: Effect<
  { offset?: number; limit?: number },
  PokedexResponse,
  void
> = createEffect()
export const loadAllPokemonsFx: Effect<
  any,
  PokedexResponse,
  void
> = createEffect()
export const loadPokemonsByTypeFx: Effect<any, any, void> = createEffect()

const recordsPaginated = createEvent<number[]>()

loadPokemonsFx.use(({ offset, limit }) => pokedexApi.getList(offset, limit))
loadAllPokemonsFx.use((count) => pokedexApi.getAll(count))
loadPokemonsByTypeFx.use((id) => pokedexApi.getByType(id))

export const $count = createStore<number>(0)
export const $paginatedPokemonsIds = createStore<number[]>([])
export const $searchPokemons = createStore<Pokemon[]>([])
export const $renderedPokemons = createStore<Pokemon[]>([])

$count.on(loadPokemonsFx.done, (_, { result }) => result.count)

$allPokemons.on(loadAllPokemonsFx.done, (_, { result }) =>
  result.results.map((pokemon) => convertUrlToId(pokemon)),
)

$currentRecords.watch((state) =>
  recordsPaginated(state.map((pokemon) => pokemon.id)),
)

$paginatedPokemonsIds.on(recordsPaginated, (_, ids) => ids)

forward({
  from: Gate.open,
  to: [loadPokemonsFx, loadPokemonsTypesFx],
})

forward({
  from: $count,
  to: loadAllPokemonsFx,
})

sample({
  source: $searchPokemonsIds,
  fn: (ids) => ids.map((id) => getFromRegistry($pokemonRegistry, id)),
  target: $searchPokemons,
})

sample({
  source: $searchPokemons,
  clock: searchTriggered,
  target: $renderedPokemons,
})

sample({
  source: $allPokemons,
  clock: [searchCleared, loadAllPokemonsFx.done],
  target: $renderedPokemons,
})
