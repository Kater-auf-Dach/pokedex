import { Search } from "@features/search"
import { Pagination } from "@features/pagination"
import { H1, MainTemplate, ThemeToggler } from "@ui"
import { $allPokemons, PokemonsList } from "@features/pokemons"
import * as React from "react"
import { useStore } from "effector-react"

import {
  $paginatedPokemonsIds,
  $renderedPokemons,
  $searchPokemons,
  Gate,
} from "./model"

export const HomePage: React.FC = () => {
  const allPokemons = useStore($allPokemons)
  const renderedPokemons = useStore($renderedPokemons)
  const searchPokemons = useStore($searchPokemons)
  const pokemonsIds = useStore($paginatedPokemonsIds)

  return (
    <>
      <Gate />
      <MainTemplate header={<Header />}>
        <Search />
        {/* <SearchResult /> */}
        {/* <PokemonsList */}
        {/*  ids={pokemonsIds} */}
        {/*  renderEmpty={() => <h2>Loading...</h2>} */}
        {/* /> */}
        {/* <Pagination list={allPokemons} /> */}

        <PokemonsList
          ids={pokemonsIds}
          renderEmpty={() => <h2>No search</h2>}
        />
        <Pagination list={renderedPokemons} />
      </MainTemplate>
    </>
  )
}

const Header = () => (
  <>
    <H1>Pokedex</H1>
    <ThemeToggler />
  </>
)
