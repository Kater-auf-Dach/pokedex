export const path = {
  home: () => "/",
  singlePokemon: (name: string) => `/pokemon/${name}`,
  singleType: (name: string) => `/types/${name}`,
}
