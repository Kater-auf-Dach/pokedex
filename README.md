## Requirements:
- List pokemons in a table view with their avatar, color, type and their attributes with customizable pagination
- Filter the pokemons with a search box
- A category tag that can filter out types of the pokemons


## Info
Use the open pokemon API (http://pokeapi.co/ *)

### Requests
Examples for request and returned data

#### Get main info
[http://pokeapi.salestock.net/api/v2/pokemon/1/](http://pokeapi.salestock.net/api/v2/pokemon/1/)

````json
{
  "name": "bulbasaur",
  "weight": 69,
  "height": 7,
  "id": 1,
  "types": [
    {
      "slot": 2, 
      "type": {
        "url": "http://pokeapi.salestock.net/api/v2/type/4/", 
        "name": "poison"
      }
    },
    {
      "slot": 1, 
      "type": {
        "url": "http://pokeapi.salestock.net/api/v2/type/12/", 
        "name": "grass"
      }
    }
  ],
  "stats": [
    {
      "stat": {
        "url": "http://pokeapi.salestock.net/api/v2/stat/6/", 
        "name": "speed"
      }, 
      "effort": 0,
      "base_stat": 45
    },
  	{
      "stat": {
        "url": "http://pokeapi.salestock.net/api/v2/stat/5/", 
        "name": "special-defense"
      }, 
      "effort": 0,
      "base_stat": 49
    },
    {
      "stat": {
        "url": "http://pokeapi.salestock.net/api/v2/stat/4/", 
        "name": "special-attack"
      }, 
      "effort": 0,
      "base_stat": 49
    },
    {
      "stat": {
        "url": "http://pokeapi.salestock.net/api/v2/stat/3/", 
        "name": "defense"
      }, 
      "effort": 0,
      "base_stat": 49
    },
    {
      "stat": {
        "url": "http://pokeapi.salestock.net/api/v2/stat/2/", 
        "name": "attack"
      },
      "effort": 0,
      "base_stat": 49
    },
    {
      "stat": {
        "url": "http://pokeapi.salestock.net/api/v2/stat/1/", 
        "name": "hp"
      },
      "effort": 0,
      "base_stat": 45
    }
  ]
}
````
#### Get species
[http://pokeapi.salestock.net/api/v2/pokemon-species/1/](http://pokeapi.salestock.net/api/v2/pokemon-species/1/)

````json
{
  "color": {"url": "http://pokeapi.salestock.net/api/v2/pokemon-color/5/", "name": "green"},
  "evolution_chain": {"url": "http://pokeapi.salestock.net/api/v2/evolution-chain/1/"}
}
````
