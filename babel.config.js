module.exports = {
  presets: ["react-app", "linaria/babel"],
  plugins: [
    "react-hot-loader/babel",
    "styled-components",
    "@babel/proposal-optional-chaining",
    ["effector/babel-plugin", { addLoc: true }],
  ],
  env: {
    development: {
      plugins: [
        [
          "styled-components",
          {
            displayName: true,
          },
        ],
      ],
    },
  },
}
